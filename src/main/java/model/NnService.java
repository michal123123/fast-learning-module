package model;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NnService {
    private static final int nEpochs = 1000;
    private static final int iterations = 1;
    private static final int seed = 12345;
    private static final double learningRate = 0.000001;
    private static int inputFeatures = 7;//+1 for label
    private static MultiLayerNetwork net;
    private static final int nSamplesToTrain = 24;
    private static final int nSamplesToGenerate = 24;
    private NormalizerMinMaxScaler normalizer = null;

    public NnService() {
        net = new MultiLayerNetwork(new NeuralNetConfiguration.Builder()
                .seed(seed)
                .iterations(iterations)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(learningRate)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.NESTEROVS)
                .list()
                .layer(0, new DenseLayer.Builder().nIn(inputFeatures).nOut(14)
                        .activation(Activation.TANH)
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(14).nOut(14)
                        .activation(Activation.TANH)
                        .build())
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .activation(Activation.IDENTITY)
                        .nIn(14).nOut(1).build())
                .pretrain(false).backprop(true).build()
        );
        net.init();
        DataSet normalizerDataSet = getNormalizerDataSet();
        normalizer = new NormalizerMinMaxScaler(1, 14);
        normalizer.fitLabel(true);
        normalizer.fit(normalizerDataSet);
    }

    public void trainNN(PlanForADay supervisedExamples, Map<LocalDateTime, Attributes> dateHourToAttributes) throws IOException {
        DataSet trainDataSet = getTrainData(supervisedExamples, dateHourToAttributes);
        for (int i = 0; i < nEpochs; i++) {
            net.fit(trainDataSet);
        }
    }

    public PlanForADay generatePlanFor(LocalDate planDate, Map<LocalDateTime, Attributes> dateHourToAttributes) {
        List<Integer> vms = new ArrayList<>();

        System.out.println("___________________________Network generates a plan::");
        for (int i = 0; i < nSamplesToGenerate; i++) {
            LocalDateTime localDateTime = LocalDateTime.of(planDate.getYear(), planDate.getMonth(), planDate.getDayOfMonth(), i, 0);
            Attributes attr = dateHourToAttributes.get(localDateTime);
            double[] rowOfFeatures = new double[inputFeatures];
            rowOfFeatures[0] = attr.getHour();
            rowOfFeatures[1] = attr.getHoliday();
            rowOfFeatures[2] = attr.getDayBetweenMondayAndFridayInclusive();
            rowOfFeatures[3] = attr.getSaturday();
            rowOfFeatures[4] = attr.getSunday();
            rowOfFeatures[5] = attr.getSummerVacation();
            rowOfFeatures[6] = attr.getAcademicYear();
            INDArray inputNDArray = Nd4j.create(rowOfFeatures, new int[]{1, inputFeatures}, 'c');//przerabia tablice z pierwsza liczba do sumy na
            INDArray predicted = net.output(inputNDArray);
            double d = predicted.getDouble(0);
            if (d < 1) {
                d = 1;
            }
            System.out.println(localDateTime.toString() + " " + Math.round(d) + " VM");
            vms.add((int) Math.round(d));
        }
        System.out.println();
        PlanForADay result = new PlanForADay(planDate, vms);
        return result;
    }

    private DataSet getTrainData(PlanForADay supervisedExamples, Map<LocalDateTime, Attributes> dateHourToAttributes) {
        double[] labels = new double[nSamplesToTrain];
        double[] rowsOfFeatures = new double[nSamplesToTrain * inputFeatures];

        LocalDate date = supervisedExamples.getDate();
        LocalDateTime localDateTime = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), 0, 0);
        int k = 0;
        for (int i = 0; i < nSamplesToTrain; i++) {
            Attributes attr = dateHourToAttributes.get(localDateTime);
            rowsOfFeatures[k++] = attr.getHour();
            rowsOfFeatures[k++] = attr.getHoliday();
            rowsOfFeatures[k++] = attr.getDayBetweenMondayAndFridayInclusive();
            rowsOfFeatures[k++] = attr.getSaturday();
            rowsOfFeatures[k++] = attr.getSunday();
            rowsOfFeatures[k++] = attr.getSummerVacation();
            rowsOfFeatures[k++] = attr.getAcademicYear();
            labels[i] = supervisedExamples.getVms().get(i);
            localDateTime = localDateTime.plusHours(1);
        }
        INDArray inputNDArray = Nd4j.create(rowsOfFeatures, new int[]{nSamplesToTrain, inputFeatures}, 'c');//przerabia tablice z pierwsza liczba do sumy na
        INDArray outPut = Nd4j.create(labels, new int[]{nSamplesToTrain, 1});
        return new DataSet(inputNDArray, outPut);
    }

    private DataSet getNormalizerDataSet() {
        double[] labels = new double[6];
        double[] rowsOfFeatures = new double[6 * inputFeatures];

        int[][] sample1 = {{0, 5, 1, 1, 0, 0, 0, 1},
                {19, 14, 0, 1, 0, 0, 0, 1},
                {21, 1, 0, 0, 1, 0, 0, 1},
                {23, 8, 1, 1, 0, 0, 0, 1},
                {23, 4, 0, 0, 1, 0, 0, 1},
                {23, 4, 0, 0, 0, 1, 1, 0}};
        int k = 0;
        for (int i = 0; i < 6; i++) {
            rowsOfFeatures[k++] = sample1[i][0];
            labels[i] = sample1[i][1];//labels
            rowsOfFeatures[k++] = sample1[i][2];
            rowsOfFeatures[k++] = sample1[i][3];
            rowsOfFeatures[k++] = sample1[i][4];
            rowsOfFeatures[k++] = sample1[i][5];
            rowsOfFeatures[k++] = sample1[i][6];
            rowsOfFeatures[k++] = sample1[i][7];
/*
0-hour
1-requests
state holiday  or church holiday(3),
day between Monday- Friday (inclusive)(4)
saturday(5)
sunday(6)
summer vacation(7)
academic year(8)
 */
        }
        INDArray inputNDArray = Nd4j.create(rowsOfFeatures, new int[]{nSamplesToTrain, inputFeatures}, 'c');//przerabia tablice z pierwsza liczba do sumy na
        INDArray outPut = Nd4j.create(labels, new int[]{nSamplesToTrain, 1});
        return new DataSet(inputNDArray, outPut);
    }

    public void saveTheNetwork(File locationToSave) throws IOException {
        //Updater: i.e., the state for Momentum, RMSProp, Adagrad etc.
        // Save this if you want to train your network more in the future
        boolean saveUpdater = true;
        if (locationToSave.exists()) {
            locationToSave.delete();
        }
        ModelSerializer.writeModel(net, locationToSave, saveUpdater);
    }
}
