package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static main.FastLearningModule.divisorForSupervisedLearning;

public class SupervisedExamplesComputer {
    public static PlanForADay compute(LocalDate planDate,
                                      Map<LocalDateTime, Integer> dateHourToCpuPercentageUsage) {
        List<Integer> vmsSupervised = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            LocalDateTime localDateTime = LocalDateTime.of(planDate.getYear(), planDate.getMonth(), planDate.getDayOfMonth(), i, 0);
            int cpuUsagePercent = dateHourToCpuPercentageUsage.get(localDateTime);
            int correctVmCount = (int) Math.round((double) cpuUsagePercent / (double) divisorForSupervisedLearning);
            if (correctVmCount < 1) {
                correctVmCount = 1;
            }
            vmsSupervised.add(correctVmCount);
        }
        return new PlanForADay(planDate, vmsSupervised);
    }
}
