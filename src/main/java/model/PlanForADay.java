package model;

import java.time.LocalDate;
import java.util.List;

public class PlanForADay {
    private final  LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public List<Integer> getVms() {
        return vms;
    }

    private final List<Integer> vms;

    public PlanForADay(LocalDate date, List<Integer> vms) {
        this.date = date;
        if (vms.size() != 24) {
            throw new IllegalArgumentException();
        }
        this.vms = vms;
    }
}
