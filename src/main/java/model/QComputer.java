package model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

import static main.FastLearningModule.lowerBound;
import static main.FastLearningModule.upperBound;

public class QComputer {
    public static int computeQ(PlanForADay plan, LocalDate startDate, Map<LocalDateTime, Attributes> dateHourToAttributes,
                               Map<LocalDateTime, Integer> dateHourToCpuPercentageUsage) {
        int qForDay = 0;
        for (int i = 0; i < 24; i++) {//for each hour
            LocalDateTime localDateTime = LocalDateTime.of(startDate.getYear(), startDate.getMonth(), startDate.getDayOfMonth(), i, 0);
            int qForHour = 0;
            int cpuUsagePercent = dateHourToCpuPercentageUsage.get(localDateTime);
            int vms = plan.getVms().get(i);
            double percentageOnSingleVm = cpuUsagePercent / vms;
            if (percentageOnSingleVm < lowerBound) {
                qForHour = vms * ((int) Math.round(lowerBound - percentageOnSingleVm));
            } else if (percentageOnSingleVm > upperBound) {
                qForHour = vms * ((int) Math.round(percentageOnSingleVm - upperBound));
            } else {
                //usage is in the right interval
            }
            qForDay += qForHour;
        }
        return qForDay;
    }
}

