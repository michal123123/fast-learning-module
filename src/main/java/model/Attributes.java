package model;

public class Attributes {
    private final byte hour;
    private final byte holiday;
    private final byte dayBetweenMondayAndFridayInclusive;
    private final byte saturday;
    private final byte sunday;
    private final byte summerVacation;
    private final byte academicYear;


    public Attributes(byte hour, byte holiday, byte dayBetweenMondayAndFridayInclusive, byte saturday,
                      byte sunday, byte summerVacation, byte academicYear) {
        this.hour = hour;
        this.holiday = holiday;
        this.dayBetweenMondayAndFridayInclusive = dayBetweenMondayAndFridayInclusive;
        this.saturday = saturday;
        this.sunday = sunday;
        this.summerVacation = summerVacation;
        this.academicYear = academicYear;
    }

    public byte getHour() {
        return hour;
    }

    public byte getHoliday() {
        return holiday;
    }

    public byte getDayBetweenMondayAndFridayInclusive() {
        return dayBetweenMondayAndFridayInclusive;
    }

    public byte getSaturday() {
        return saturday;
    }

    public byte getSunday() {
        return sunday;
    }

    public byte getSummerVacation() {
        return summerVacation;
    }

    public byte getAcademicYear() {
        return academicYear;
    }
}
