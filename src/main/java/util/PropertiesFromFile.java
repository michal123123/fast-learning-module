package util;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class PropertiesFromFile {
    private static Logger LOGGER = Logger.getLogger(PropertiesFromFile.class);
    private File propFile;
    private static Map<String, String> propertiesMap;
    private static PropertiesFromFile singleton;

    public static String NETWORK_ID = "networkId";
    public static String IDENTITY_ENDPOINT = "identityEndpoint";
    public static String RESOURCE_ENDPOINT = "resourceInstance";
    public static String METRICS_ENDPOINT = "metricsEndpoint";
    public static String R_MIN = "rmin";
    public static String R_MAX = "rmax";

    private PropertiesFromFile(File propFile) {
        if (propFile == null || !propFile.exists()) {
            String msg = "Invalid properties file";
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg);
        }
        propertiesMap = new HashMap<>();
        this.propFile = propFile;
        readFile();
        if (propertiesMap.size() != 6) {
            String msg = "Not enough properties";
            LOGGER.error(msg);
            throw new IllegalStateException(msg);
        }
    }

    public static synchronized PropertiesFromFile getInstance(File propFile) {
        if (singleton != null) {
            return singleton;
        }
        singleton = new PropertiesFromFile(propFile);
        return singleton;
    }

    public static String getProperty(String propName) {
        if (propertiesMap == null) {
            throw new IllegalStateException();
        }
        return propertiesMap.get(propName);
    }

    private void readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(propFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] attributes = line.split(",");
                if (attributes.length != 2) {//single line should have 2 parts
                    String msg = "Invalid syntax in propeties file";
                    LOGGER.error(msg);
                    throw new IllegalArgumentException(msg);
                }
                String name = attributes[0];
                String value = attributes[1];
                if (value != null) {
                    propertiesMap.put(name, value);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            throw new IllegalArgumentException();
        }
    }
}