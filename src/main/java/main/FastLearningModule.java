package main;

import model.NnService;
import model.PlanForADay;
import model.QComputer;
import model.SupervisedExamplesComputer;
import model.Attributes;
import util.UsageFileToMaps;
import util.PropertiesFromFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FastLearningModule {
    public static String WORKSPACE = System.getenv("OPENSTACK_WORKSPACE");
    public static int lowerBound;
    public static int upperBound;
    public static int divisorForSupervisedLearning;

    public static void main(String[] args) throws IOException {
        boolean alphaParameter = false;
        double alphaValue = 0.1;//disabled
        File propertiesFile = new File(WORKSPACE+"/module_c_properties");
        System.out.println("Loading properties from file " + propertiesFile.getAbsolutePath());
        PropertiesFromFile.getInstance(propertiesFile);
        lowerBound = Integer.parseInt(PropertiesFromFile.getProperty(PropertiesFromFile.R_MIN));
        upperBound = Integer.parseInt(PropertiesFromFile.getProperty(PropertiesFromFile.R_MAX));
        divisorForSupervisedLearning = (lowerBound+upperBound) /2;

        UsageFileToMaps.loadMaps();
        Map<LocalDateTime, Attributes> dateHourToAttributes = UsageFileToMaps.hourToAttributes;
        Map<LocalDateTime, Integer> dateHourToCpuPercentageUsage = UsageFileToMaps.hourToCpuPercentageUsage;
        PlanForADay[] plans = new PlanForADay[365];
        plans[0] = getFirstPlan();
        int[] qForEachDay = new int[365];
        LocalDate planDate = LocalDate.of(2018, Month.JANUARY, 1);
        NnService nnService = new NnService();
        for (int i = 0; i < 365; i++) {
            qForEachDay[i] = QComputer.computeQ(plans[i], planDate, dateHourToAttributes, dateHourToCpuPercentageUsage);
            PlanForADay supervisedExamples = SupervisedExamplesComputer.compute(planDate, dateHourToCpuPercentageUsage);
            nnService.trainNN(supervisedExamples, dateHourToAttributes);
            if (i != 364) {
                planDate = planDate.plusDays(1);
                PlanForADay generatedForNextDay = nnService.generatePlanFor(planDate, dateHourToAttributes);
                if (alphaParameter && i > 0) {
                    plans[i + 1] = mergeTwoPlans(plans[i], generatedForNextDay, alphaValue);
                } else {
                    plans[i + 1] = generatedForNextDay;
                }
            }
        }
        int qTotal = 0;
        for (int i = 0; i < 365; i++) {
            qTotal +=qForEachDay[i];
            System.out.println(qForEachDay[i]);
        }
        System.out.println();
        System.out.println("Q total:"+qTotal);
        nnService.saveTheNetwork(new File(WORKSPACE+File.separator+"network_dl4j.zip"));
    }

    private static PlanForADay mergeTwoPlans(PlanForADay plan, PlanForADay generatedForNextDay, double alphaValue) {
        List<Integer> mergedVms = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            int oldVal = plan.getVms().get(i);
            int newVal = generatedForNextDay.getVms().get(i);
            int diff = newVal - oldVal;
            int mergedVal = oldVal + (int) Math.round(alphaValue * ((double) diff));
            if (mergedVal < 1) {
                mergedVal = 1;
            }
            mergedVms.add(mergedVal);
        }
        return new PlanForADay(generatedForNextDay.getDate(), mergedVms);
    }

    private static void displaySupervisedExample(PlanForADay supervisedExamples) {
        System.out.println("Supervised examples:\n");
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                System.out.println(supervisedExamples.getDate().toString() + "T0" + i + ":00 " + supervisedExamples.getVms().get(i) + " VM");
            } else {
                System.out.println(supervisedExamples.getDate().toString() + "T" + i + ":00 " + supervisedExamples.getVms().get(i) + " VM");
            }
        }
    }

    private static PlanForADay getFirstPlan() {
        LocalDate startDate = LocalDate.of(2018, Month.JANUARY, 1);
        List<Integer> firstDayVms = new ArrayList<>();
        for (int k = 0; k < 24; k++) {
            firstDayVms.add(8);
        }
        PlanForADay first = new PlanForADay(startDate, firstDayVms);
        return first;
    }
}
